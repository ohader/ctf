Name:
    <The name of the Challenge>
Description:
    <Short Description of the Challenge>
Files:
    <File uploaded in the Framework>
Hints:  
    <One Hint | Amount of Points>
    <Another Hint | Amount of Points>
Category:
    <Category of the Challenge>
Points/Value:
    <Amount of Points>
Flag:
    <THICTF{YourFlagHere}>