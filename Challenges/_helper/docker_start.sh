#! /usr/bin/bash

base_path=`pwd`
base_path="${base_path}/../"


## Web
web='Web/'
path_web="${base_path}""${web}"


### Next Try
next_try='Next_Try'

docker build -t thi_ctf_19/next_try "${path_web}${next_try}"
docker run -it -d -p 5001:5000 thi_ctf_19/next_try

### HTTP_Server - currently deactived because it's not working as expected
http_server='HTTP_Server'

#docker build -t thi_ctf_19/http_server "${path_web}${http_server}"
#docker run -it -d -p 5002:5000 thi_ctf_19/http_server

## Reverse Engineering
reverse='Reverse_Engineering/'
path_reverse="${base_path}""${reverse}"


### Reverse1
reverse1='reverse1/'

docker build -t thi_ctf_19/reverse1 "${path_reverse}${reverse1}"
docker run -itd -p 2223:22 thi_ctf_19/reverse

### Strings
strings='strings/'

docker build -t thi_ctf_19/strings "${path_reverse}${strings}"
docker run -itd -p 2224:22 thi_ctf_19/strings

### Strings-Reloaded
stringsReloaded='strings-reloaded/'

docker build -t thi_ctf_19/strings-reloaded "${path_reverse}${stringsReloaded}"
docker run -itd -p 2225:22 thi_ctf_19/strings-reloaded


## Misc
misc='Misc/'
path_misc="${base_path}""${misc}"


### Ssh Connection
ssh='ssh_connection/'

docker build -t thi_ctf_19/ssh_connection "${path_misc}${ssh}"
docker run -itd -p 2222:22 thi_ctf_19/ssh_connection



## Binary Exploitation
binary='Binary_Exploitation/'
path_binary="${base_path}""${binary}"


### Buffer Overflow 1
bufferOverflow1='BufferOverflow1/'

docker build -t thi_ctf_19/buffer_overflow1 "${path_binary}${bufferOverflow1}"
docker run -itd -p 2226:22 thi_ctf_19/buffer_overflow1


### Mystery String
mysteryString='Mystery_String/'

docker build -t thi_ctf_19/mystery_string "${path_binary}${mysteryString}"
docker run -itd -p 2227:22 thi_ctf_19/mystery_string


### Injection
injection='Injection/'

docker build -t thi_ctf_19/injection "${path_binary}${injection}"
docker run -itd -p 2228:22 thi_ctf_19/injection
