Name:
    Meta
Description:
    Another Image for you. Take a look!
Files:
    - image.jpg
Hints:  
    /
Category:
    Forensics
Points/Value:
    60
Flag:
    THICTF{metas_are_so_interesting_48.77_11.43_Ingolstadt}