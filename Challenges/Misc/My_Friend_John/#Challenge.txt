Name:
    My_Friend_John
Description:
    That guy named "John" handed me this. Can you help?
Files:
    secret.thictf
Hints:  
    /
Category:
    Misc
Points/Value:
    90
Flag:
    THICTF{you_should_learn_to_use_proper_passwords}
Comment:
	New Password for zip is "dragon"