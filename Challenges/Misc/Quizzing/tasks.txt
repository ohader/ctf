1. What does the following code snippet represent?

<element name="AuthnStatement" type="saml:AuthnStatementType"/>
...

a) SAML Statement	
b) SAML Authentication statement		
c) SAML Assertion	
d) SAML Attribute statement		


2. What would be a way for a spammer to find valid E-Mail addresses?

a) Directory Harvest Attack
b) SMTP poisioning 		
c) DNS poisioning			
d) SMTP spoofing


3. Buffer Overflow: What do you call a succession of instructions meant to boost the hit ratio of a desired memory address for malicious code?

a) Malware operation		
b) Dead-Code-Insertion	
c) Obfuscation			
d) NOP-slide			


4. Buffer Overflow with C function 'strcpy': What do you have to remove within the shellcode or otherwise it doesn't copy all the data it's supposed to copy? 

a) 0x11				
b) 0x08				
c) 0x00				
d) 0x0f				


5. Which attack is possible on the following piece of code?

...
// userName and userAccount are from a web form
String userName = request.getParameter("userName");
String userAccount = request.getParameter("userAccount");

String query = "SELECT * FROM user WHERE user_name=" + userName + " AND userAccount=?";

PreparedStatement p = connection.prepareStatement(query);
p.setString(1,userAccount);
ResultSet results = p.executeQuery();
...

a) SQL Injection	
b) DB-Injection		
c) Cross-Site Scripting
d) Data Breach			


6. What hashing algorithm should NOT be used anymore in 2019?

a) SHA-3
b) SHA-2
c) MD5


7. Picture 'security_principle.PNG': Which principle of security is infringed?

a) Be reluctant to trust
b) Minimize attack surface area	
c) Fail securely			
d) Keep security simple		


