Name:
    Quizzing
Description:
    The flag will be composed of the small letters (in order) representing the answers. The format is "THICTF{(the letters)}". If, for example, the answers to questions 1. to 6. would be a) and the last answer would be d), the flag would be: THICTF{aaaaaad}
    Attention, you only get three flag-submit attempts, so trying out many combinations will be your doom.
Files:
    tasks.txt
Hints:  
    /
Category:
    Misc
Points/Value:
    350
Flag:
    THICTF{badcaca}