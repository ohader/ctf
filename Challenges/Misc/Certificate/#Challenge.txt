Name:
    Certificate
Description:
    Something seems to be wrong with this certificate...
Files:
    Certificate.crt
Hints:  
    /
Category:
    Misc
Points/Value:
    30
Flag:
    THICTF{encoding_is_not_encryption}