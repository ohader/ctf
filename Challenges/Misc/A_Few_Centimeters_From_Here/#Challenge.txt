Name:
    A_Few_Centimeters_From_Here
Description:
    What's the binary distance between "Hamming" and "technic"? Do I need a ruler?


    Design your flag like this: THICTF{answer!answer!answer!}

Files:
    no Files needed
Hints:  
    /
Category:
    Misc
Points/Value:
    40
Flag:
    THICTF{17!17!17!}