Name:
    Crypted_Joke
Description:
    Hey man, I can't read the text in the image <Link>. 
    Maybe I need a key or something else? 
    Can you help?
Files:
    - eggs.jpg
Hints:  
    /
Category:
    Cryptography
Points/Value:
    80
Flag:
    THICTF{THEY_HAD_EGGS}