Name:
    assembler0
Description:
    What does asm0(0xde, 0xex) return? Flag is in the following format:
    THICTF{<return value in hex}
Files:
    - assembly.S
Hints:  
    /
Category:
    Reverse Engineering
Points/Value:
    90
Flag:
    THICTF{0x00}