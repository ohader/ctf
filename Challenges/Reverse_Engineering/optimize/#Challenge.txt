Name:
    optimize
Description:
    We hired a student to calculate this flag. He did it successfully, but it's kinda slow.
    So now we hired you to get the flag (without payment of cource ;)
Files:
    CalcFlag (binary) or CalcFlag.c
Hints:  
    - Recover the C-Code | 50
    - check the sieveOfEratosthenes | 200
Category:
    Reverse Engineering
Points/Value:
    400
Flag:
    THICTF{2000107565}      //output of optimized C-Code
    THICTF{2000107564}      //output of sieveOfEratosthenes
