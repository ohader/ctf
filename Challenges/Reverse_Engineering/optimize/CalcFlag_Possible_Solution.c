#include<stdio.h>
#include<string.h>

typedef int bool;
#define true 1
#define false 0

int prime(b){
    if(b % 100 == 0){
        printf("%d \n", b);
        return 0;
    }
    for(int i = 2; i<b; i++){
        if(b%i == 0){
            return 0;
        }
    }
    return b;
}

int fun_C(){
    int a = 0;
    for(int b = 1; a < 2000000000; b++){
        a += prime(b);
    }
    return a;
}



int main() {

    printf("THICTF{%i}", fun_C());
	return 0;
}
